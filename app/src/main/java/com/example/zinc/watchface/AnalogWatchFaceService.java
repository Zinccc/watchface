package com.example.zinc.watchface;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.res.Resources;
import android.graphics.Bitmap;
import android.graphics.Canvas;
import android.graphics.Paint;
import android.graphics.Rect;
import android.graphics.drawable.BitmapDrawable;
import android.graphics.drawable.Drawable;
import android.os.Handler;
import android.os.Message;
import android.os.Vibrator;
import android.support.wearable.watchface.CanvasWatchFaceService;
import android.support.wearable.watchface.WatchFaceStyle;
import android.text.format.Time;
import android.util.DisplayMetrics;
import android.util.Log;
import android.view.SurfaceHolder;

import java.util.Date;
import java.util.TimeZone;
import java.util.Timer;
import java.util.TimerTask;
import java.util.concurrent.TimeUnit;

public class AnalogWatchFaceService extends CanvasWatchFaceService {

    @Override
    public Engine onCreateEngine() {
        return new Engine(this);
    }

    private class Engine extends CanvasWatchFaceService.Engine {
        static final int MSG_UPDATE_TIME = 0;

        int characterFrameNumber = 10;
        int currentFrame = 0;

        DisplayMetrics metrics = getApplicationContext().getResources().getDisplayMetrics();
        int width = metrics.widthPixels;
        int height = metrics.heightPixels;

        /* 200밀리세컨드(1초에 5번) 단위로 업데이트트 */
        final long INTERACTIVE_UPDATE_RATE_MS = TimeUnit.MILLISECONDS.toMillis(1000 / 5);
        final long LEVEL_CHECK_RATE_MS = TimeUnit.MILLISECONDS.toMillis(1000 * 3600);
        final long MOVE_CHECK_RATE_MS = TimeUnit.MILLISECONDS.toMillis(1000 * 360);
        static final int RECOMMENDED_HOURLY_STEPS = 120;    //권장 한시간 걸음수 test 용으로 120넣음.
        static final int NUM_GAUGE_LEVEL = 10;


        /* 타임 객체 */
        Time mTime;
        private static final int START_HOUR = 10;
        private static final int END_HOUR = 18;

        /* 그래픽 객체 */
        Bitmap mBackgroundBitmap;
        Bitmap mBackgroundScaledBitmap;

        Bitmap mGaugeBitmap;
        Bitmap mGaugeScaledBitmap;

        Bitmap[] mCharacterBitmap;
        Bitmap[] mCharacterScaledBitmap;

        Bitmap[] mNumberBitmap;
        Bitmap[] mNumberScaledBitmap;

        Bitmap mColonBitmap;
        Bitmap mColonScaledBitmap;

        /* 캐릭터 리소스 */
        final int[] charRes = {
                R.drawable.char1,
                R.drawable.char2,
                R.drawable.char3,
                R.drawable.char4,
                R.drawable.char5,
                R.drawable.char6,
                R.drawable.char7,
                R.drawable.char8,
                R.drawable.char9,
                R.drawable.char10,
        };

        /* 디지털 시계 숫자 리소스 */
        final int[] numRes = {
                R.drawable.num0,
                R.drawable.num1,
                R.drawable.num2,
                R.drawable.num3,
                R.drawable.num4,
                R.drawable.num5,
                R.drawable.num6,
                R.drawable.num7,
                R.drawable.num8,
                R.drawable.num9
        };

        /* 디지털 시계를 그리기 위한 객체*/
        Paint mDefaultPaint;
        Paint mColonClearedPaint;


        /* 만보계를 위한... */
        private CanvasWatchFaceService canvasWatchFaceService;
        private StepCount stepCount;

        /* 진동 스레드를 위한...*/
        private Timer vibratorTimer;
        private Vibrator vibrator;
        final long[] vibrationPattern = {50, 500, 50, 300};

        /*움직임 체크 스레드를 위한...*/
        private Timer moveCheckTimer;


        /* Interactive 모드일 때, 1초에 다섯번 시간을 업데이트 하기 위해 사용하는 핸들러 */
        final Handler mUpdateTimeHandler = new Handler() {
            @Override
            public void handleMessage(Message message) {
                switch (message.what) {
                    case MSG_UPDATE_TIME:
                        invalidate();
                        /* Interactive 모드이면 */
                        if (isVisible() && !isInAmbientMode()) {
                            long timeMs = System.currentTimeMillis();
                            long delayMs = INTERACTIVE_UPDATE_RATE_MS - (timeMs % INTERACTIVE_UPDATE_RATE_MS);
                            mUpdateTimeHandler.sendEmptyMessageDelayed(MSG_UPDATE_TIME, delayMs);
                        }
                        break;
                }
            }
        };

        /* Timezone의 변경을 감지하는 리시버 */
        final BroadcastReceiver mTimeZoneReceiver = new BroadcastReceiver() {
            @Override
            public void onReceive(Context context, Intent intent) {
                mTime.clear(intent.getStringExtra("time-zone"));
                mTime.setToNow();
            }
        };

        /* Timezone 리시버의 등록 여부를 저장하는 변수 */
        boolean mRegisteredTimeZoneReceiver = false;

        Engine(CanvasWatchFaceService canvasWatchFaceService) {
            this.canvasWatchFaceService = canvasWatchFaceService;
        }

        //혜미로부터기대하는 공유변수들
        private int moveLevel = 3;
        int minute = 0;

        @Override
        public void onCreate(SurfaceHolder holder) {
            super.onCreate(holder);
            /* 워치페이스 초기화시 호출되는 콜백 */
            /* System UI를 설정합니다 */

            Resources resources = AnalogWatchFaceService.this.getResources();

            /* 이미지를 로드 */
            Drawable backgroundDrawable = resources.getDrawable(R.drawable.background);
            mBackgroundBitmap = ((BitmapDrawable) backgroundDrawable).getBitmap();

            Drawable gaugeDrawable = resources.getDrawable(R.drawable.gauge);
            mGaugeBitmap = ((BitmapDrawable) gaugeDrawable).getBitmap();

            Drawable colonDrawable = resources.getDrawable(R.drawable.colon);
            mColonBitmap = ((BitmapDrawable) colonDrawable).getBitmap();


            /* 캐릭터 스프라이트 로드 */
            Drawable characterDrawable;
            mCharacterBitmap = new Bitmap[characterFrameNumber];
            mCharacterScaledBitmap = new Bitmap[characterFrameNumber];
            for (int i = 0; i < characterFrameNumber; i++) {
                characterDrawable = resources.getDrawable(charRes[i]);
                mCharacterBitmap[i] = ((BitmapDrawable) characterDrawable).getBitmap();
            }

            /* 숫자 리소스 로드 */
            Drawable numberDrawable;
            mNumberBitmap = new Bitmap[numRes.length];
            mNumberScaledBitmap = new Bitmap[numRes.length];
            for (int i = 0; i < numRes.length; i++) {
                numberDrawable = resources.getDrawable(numRes[i]);
                mNumberBitmap[i] = ((BitmapDrawable) numberDrawable).getBitmap();
            }

            resizeImages();

            mDefaultPaint = new Paint();
            mDefaultPaint.setAntiAlias(true);

            mColonClearedPaint = new Paint();
            mColonClearedPaint.setARGB(180, 255, 255, 255);
            mColonClearedPaint.setAntiAlias(true);

            /* 타임 객체를 생성합니다 */
            mTime = new Time();
            mTime.setToNow();
            /* System UI를 설정합니다 */
            setWatchFaceStyle(new WatchFaceStyle.Builder(AnalogWatchFaceService.this)
                    .setCardPeekMode(WatchFaceStyle.PEEK_MODE_SHORT)
                    .setBackgroundVisibility(WatchFaceStyle.BACKGROUND_VISIBILITY_INTERRUPTIVE)
                    .setShowSystemUiTime(false)
                    .build());

            /* 만보기 객체를 생성합니다. */
            stepCount = new StepCount(this, canvasWatchFaceService, RECOMMENDED_HOURLY_STEPS);

            /*level check 스레드를 생성합니다. (기능2에 해당)
             * 1. 6분마다 (6분당 추천걸음수)와 6분동안 걸음수 를 비교
               2. move level 값 조정
            */

            Date next6Minutes = new Date(mTime.toMillis(true) - (mTime.toMillis(true) % 360000) + 360000);
            Log.d("moveChecktimer", "current :" + new Date(mTime.toMillis(true)).toString() + "next6Minutes: " + next6Minutes);


            moveCheckTimer = new Timer();
            moveCheckTimer.schedule(new TimerTask() {
                private static final int MAX_MOVE_LEVEL = 11;
                private static final int MIN_MOVE_LEVEL = 0;
                int minute = 0;
                int recommendedLevellyStepCount = stepCount.getRecommendedHourlyStepCount() / NUM_GAUGE_LEVEL; // 6분당 추천걸음수 계산.

                @Override
                public void run() {
                    //움직임을 체크
                    mTime.setToNow();
                    minute = mTime.minute; //6을빼줘야하는 이유? 과거 시간의 기록을 보는 거니깐... ex) 48분에 점검하면, 42~47분사이 걸음을 봄
                    minute = (minute == 0) ? minute = 59 : minute - 1;
                    Log.d("moveCheckTimer", "fixed minute " + minute + "\t arrayNum " + minute / 6 + "\t current move level " + moveLevel);

                    if (MIN_MOVE_LEVEL > moveLevel || moveLevel > MAX_MOVE_LEVEL) {
                        // 불가능한 경우이지만 혹시나 해서 인자 체크
                        Log.d("moveCheckTimer", "impposible case!!!! check moveLevel :" + moveLevel);
                    }
                    if (stepCount.getLevellyStepCount(minute) < recommendedLevellyStepCount) { // Up case
                        moveLevel = (moveLevel < MAX_MOVE_LEVEL) ? moveLevel + 1 : MAX_MOVE_LEVEL;
                        Log.d("moveCheckTimer", "up case check moveLevel :" + moveLevel);
                    }
                    stepCount.resetLevellyStepCount(minute);
                }
            }, next6Minutes, MOVE_CHECK_RATE_MS);


            /* 진동 스레드를 생성합니다.(기능3에 해당 ) */
            vibrator = (Vibrator) canvasWatchFaceService.getSystemService(Context.VIBRATOR_SERVICE);
            vibratorTimer = new Timer();
            // 3600000 == 1hour, to get next hour
            Date nextHour = new Date(mTime.toMillis(true) - (mTime.toMillis(true) % 3600000) + 3600000);
            //위 작업은 (예) 13:03분에 시계를 켰을 경우 스레드 시작 시점이 14:00 정각으로 맞춰지기 위함이다.
            vibratorTimer.schedule(new TimerTask() {
                private static final int VIBRATOR_MOVE_LEVEL = 8; // 레벨8이상이면 진동
                private static final int INDEX_IN_PATTERN_TO_REPEAT = -1;

                //                private static final int START_HOUR = 10;
//                private static final int END_HOUR = 18;
                public void run() {

                    // 진동스레드(기능3)가 하는 일
                    // 1. 근무시간(10시~18시)인지 먼저 확인하고
                    // 2. - 움직임의 레벨이 8이상이면 진동을 울린다.
                    // 3. - 그렇지 않으면 아무런 이벤트 발생X

                    mTime.setToNow();
                    if (START_HOUR <= mTime.hour && mTime.hour <= END_HOUR) {
                        if (moveLevel >= VIBRATOR_MOVE_LEVEL) {
                            vibrator.vibrate(vibrationPattern, INDEX_IN_PATTERN_TO_REPEAT);
                        }
                    }
                }
            }, nextHour, LEVEL_CHECK_RATE_MS); // set in nextHour first, and every hour after.

        }// End of onCreate()


        public void resizeImages() {
               /* 배경 이미지의 크기를 bound에 맞게 수정하고 배경으로 지정합니다. */
            if (mBackgroundScaledBitmap == null
                    || mBackgroundScaledBitmap.getWidth() != width
                    || mBackgroundScaledBitmap.getHeight() != height) {
                mBackgroundScaledBitmap = Bitmap.createScaledBitmap(mBackgroundBitmap,
                        width, height, true);
            }
                /* 이미지 리사이징 */
            if (mGaugeScaledBitmap == null
                    || mGaugeScaledBitmap.getWidth() != width
                    || mGaugeScaledBitmap.getHeight() != height) {
                mGaugeScaledBitmap = Bitmap.createScaledBitmap(mGaugeBitmap,
                        width, (height * 11) / 10, true);
            }
            if (mColonScaledBitmap == null) {
                mColonScaledBitmap = Bitmap.createScaledBitmap(mColonBitmap,
                        width / 32, (int) (height / 10.7), true);
            }

            /* 캐릭터 이미지 리사이징 */
            if (mCharacterScaledBitmap[0] == null) {
                for (int i = 0; i < characterFrameNumber; i++) {
                    mCharacterScaledBitmap[i] = Bitmap.createScaledBitmap(mCharacterBitmap[i],
                            (int) (width / 3.5), height / 2, true);
                }
            }

            /* 숫자 리소스 리사이징 */
            if (mNumberScaledBitmap[0] == null) {
                for (int i = 0; i < numRes.length; i++) {
                    mNumberScaledBitmap[i] = Bitmap.createScaledBitmap(mNumberBitmap[i],
                            (int) (width / 10.7), (int) (height / 6.4), true);
                }
            }
        }

        private void updateTimer() {
            mUpdateTimeHandler.removeMessages(MSG_UPDATE_TIME);
            if (shouldTimerBeRunning()) {
                mUpdateTimeHandler.sendEmptyMessage(MSG_UPDATE_TIME);
            }
        }

        private boolean shouldTimerBeRunning() {
            return isVisible() && !isInAmbientMode();
        }

        @Override
        public void onVisibilityChanged(boolean visible) {
            super.onVisibilityChanged(visible);

            if (visible) {
                registerReceiver();

                // Update time zone in case it changed while we weren't visible.
                mTime.clear(TimeZone.getDefault().getID());
                mTime.setToNow();
            } else {
                unregisterReceiver();
            }

            // Whether the timer should be running depends on whether we're visible and
            // whether we're in ambient mode), so we may need to start or stop the timer
            updateTimer();
        }

        private void registerReceiver() {
            if (mRegisteredTimeZoneReceiver) {
                return;
            }
            mRegisteredTimeZoneReceiver = true;
            IntentFilter filter = new IntentFilter(Intent.ACTION_TIMEZONE_CHANGED);
            AnalogWatchFaceService.this.registerReceiver(mTimeZoneReceiver, filter);
        }

        private void unregisterReceiver() {
            if (!mRegisteredTimeZoneReceiver) {
                return;
            }
            mRegisteredTimeZoneReceiver = false;
            AnalogWatchFaceService.this.unregisterReceiver(mTimeZoneReceiver);
        }

        @Override
        public void onTimeTick() {
            super.onTimeTick();
            /* Ambient 모드에서 1분마다 호출되는 콜백 */
            invalidate();
        }

        @Override
        public void onDestroy() {
            super.onDestroy();
            vibratorTimer.cancel();
            moveCheckTimer.cancel();
            /* 워치페이스 종료시 호출되는 콜백 */
        }

        @Override
        public void onAmbientModeChanged(boolean inAmbientMode) {
            super.onAmbientModeChanged(inAmbientMode);
            /* 워치페이스의 모드가 변경될 때 호출되는 콜백 */
        }

        @Override
        public void onDraw(Canvas canvas, Rect bounds) {
            super.onDraw(canvas, bounds);
            canvas.save();

            /* 시간을 갱신합니다. */
            mTime.setToNow();
            int hour = mTime.hour;
            int min = mTime.minute;
            int sec = mTime.second;
            int minLeft;
            int minRight;

            int width = bounds.width();
            int height = bounds.height();

            /* 화면 높이를 십등분한 값 */
            int levelUnit = height / 10;

            //moveLevel Down Case
            minute = min;
//            minute = (minute < 6) ? minute = 59 : minute - 6;
            Log.d("moveCheckTimer_down case", "fixed minute " + minute + "\t arrayNum " + minute / 6 + "\t current move level " + moveLevel);
            int recommendedLevellyStepCount = stepCount.getRecommendedHourlyStepCount() / NUM_GAUGE_LEVEL; // 6분당 추천걸음수 계산.

            if (stepCount.getLevellyStepCount(minute) >= recommendedLevellyStepCount) { // Down case
                moveLevel -= stepCount.getLevellyStepCount(minute) / recommendedLevellyStepCount;
                moveLevel = (moveLevel < 0) ? 0 : moveLevel;
                stepCount.resetLevellyStepCount(minute);
                Log.d("moveCheckTimer_down case", "down case check moveLevel :" + moveLevel);
            }

            canvas.drawBitmap(mBackgroundScaledBitmap, 0, 0, null);
            canvas.drawBitmap(mGaugeScaledBitmap, 0, height - levelUnit * moveLevel, null);


            /* 캐릭터 이미지 드로우 */
            canvas.drawBitmap(mCharacterScaledBitmap[currentFrame % 10], width / 3, (int) (height / 5.3), null);
            currentFrame++;

            /* 디지털 시계 */
            String hourStr = hour + "";
            if (hourStr.length() < 2) {
                hourStr = "0" + hourStr;
            }
            canvas.drawBitmap(mNumberScaledBitmap[hourStr.charAt(0) - 48], (int) (width / 4.27), (int) (height / 1.33), null);
            canvas.drawBitmap(mNumberScaledBitmap[hourStr.charAt(1) - 48], (int) (width / 2.78), (int) (height / 1.33), null);

            if (sec % 2 == 0) {
                canvas.drawBitmap(mColonScaledBitmap, (int) (width / 2.06), (int) (height / 1.28), mColonClearedPaint); //콜론
            } else
                canvas.drawBitmap(mColonScaledBitmap, (int) (width / 2.06), (int) (height / 1.28), null);
            if (min < 10) {
                canvas.drawBitmap(mNumberScaledBitmap[0], (int) (width / 1.83), (int) (height / 1.33), null);
                canvas.drawBitmap(mNumberScaledBitmap[min], (int) (width / 1.45), (int) (height / 1.33), null);
            } else {
                minLeft = min / 10;
                minRight = min % 10;
                canvas.drawBitmap(mNumberScaledBitmap[minLeft], (int) (width / 1.83), (int) (height / 1.33), null);
                canvas.drawBitmap(mNumberScaledBitmap[minRight], (int) (width / 1.48), (int) (height / 1.33), null);
            }

            canvas.drawText("step : " + stepCount.getLevellyStepCount(min) + "\n level : " + moveLevel, 10, 10, mDefaultPaint);
            canvas.restore();
        }
    }
}


