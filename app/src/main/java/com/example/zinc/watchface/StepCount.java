package com.example.zinc.watchface;

import android.content.Context;
import android.hardware.Sensor;
import android.hardware.SensorEvent;
import android.hardware.SensorEventListener;
import android.hardware.SensorManager;
import android.support.wearable.watchface.CanvasWatchFaceService;
import android.text.format.Time;
import android.util.Log;

/**
 * Created by cheolmin on 2015-03-03.
 */


public class StepCount implements SensorEventListener {
    private static final int STEP_CHECK_DURATION = 6; // every 6 min, check steps
    /* 만보계 기능을 위한 객체들 */
    private SensorManager sensorManager;
    private Sensor countSensor;

    /* 걸음수 관련 객체들 */
    private int levellyStepCount[];
    private int recommendedHourlyStepCount; // 권장 1시간 걸음 수

    /* 기타 객체들 */
    private CanvasWatchFaceService.Engine canvasWatchFaceServiceEngine;
    private CanvasWatchFaceService canvasWatchFaceService;
    private Time currentTime;


    //생성자
    StepCount(CanvasWatchFaceService.Engine canvasWatchFaceServiceEngine, CanvasWatchFaceService canvasWatchFaceService, int recommendedHourlyStepCount) {
        this.canvasWatchFaceServiceEngine = canvasWatchFaceServiceEngine;
        this.canvasWatchFaceService = canvasWatchFaceService;
        this.recommendedHourlyStepCount = recommendedHourlyStepCount;
        levellyStepCount = new int[60 / STEP_CHECK_DURATION]; // 60분/레벨수

        /* 객체들을 초기화합니다.*/
        currentTime = new Time();

        /* 만보계 객체 생성, 리스너 등록 */
        setupStepCount();
    }

    // 만보계 객체를 생성하고 리스너를 등록합니다
    private boolean setupStepCount() {
        sensorManager = (SensorManager) canvasWatchFaceService.getSystemService(Context.SENSOR_SERVICE);
        countSensor = sensorManager.getDefaultSensor(Sensor.TYPE_STEP_COUNTER);
        sensorManager.registerListener(this, countSensor, SensorManager.SENSOR_DELAY_FASTEST);
        // 만보계 값은 실시간으로 반영되지 않습니다. 시스템 상황에 따라서 시간은 불규치적으로 반영됨
        // SensorManager.SENSOR_DELAY_FASTEST가 그래도 가장 빠르게 만보계 값을 반영하는 FLAG입니다.
        return true;
    }

    public int getLevellyStepCount(int minute) {
        return levellyStepCount[minute / STEP_CHECK_DURATION];
    }

    public void resetLevellyStepCount(int minute) {
        levellyStepCount[minute / STEP_CHECK_DURATION] = 0;
    }

    public int getRecommendedHourlyStepCount() {
        return recommendedHourlyStepCount;
    }

    // SensorEventListener를 위한 오버라이딩 메소드들 구현
    @Override
    public void onSensorChanged(SensorEvent sensorEvent) {
        // 만보계 움직임이 감지되면 호출되는 리스너
        // 주로 만보계 수치들을 증가시키는 동작
        currentTime.setToNow();
        levellyStepCount[currentTime.minute / STEP_CHECK_DURATION]++;
    }

    @Override
    public void onAccuracyChanged(Sensor sensor, int i) {
        // 뭐하는 메소드인지 아직 정체불명. 우리 프로젝트와 상관이 없을 것 같음.
        Log.d("TAG", "accuracy changed: " + i);
    }

}